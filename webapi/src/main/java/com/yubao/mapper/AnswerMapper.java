package com.yubao.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yubao.entity.Answer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubao.response.AnswerViewModel;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yubaolee
 * @since 2020-04-15
 */
public interface AnswerMapper extends BaseMapper<Answer> {
    Page<AnswerViewModel> queryAnswersOfQuestion(Page<AnswerViewModel> answerViewModelPage, @Param("answerto") String answerto);
}
