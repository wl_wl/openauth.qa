package com.yubao.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yubao.response.AnswerViewModel;
import com.yubao.response.QuestionViewModel;
import com.yubao.entity.Question;
import com.yubao.entity.QuestionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionMapper  extends BaseMapper<Question> {

    QuestionViewModel getQuestionVM(String id);

    List<QuestionViewModel> getQuestionVMs(QuestionExample example);

    int countByExample(QuestionExample example);

    int updateByExampleSelective(@Param("record") Question record, @Param("example") QuestionExample example);

    Page<QuestionViewModel> getUserAnswered(Page<QuestionViewModel> questionViewModelPage, @Param("uid") String uid);
}