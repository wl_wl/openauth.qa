package com.yubao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yubaolee
 * @since 2020-04-15
 */
@ApiModel(value="Answer对象", description="")
public class Answer implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("Id")
    private String id;

    @TableField("Content")
    private String content;

    @TableField("SupportCnt")
    private Integer supportCnt;

    @TableField("OpposeCnt")
    private Integer opposeCnt;

    @TableField("AnswerTo")
    private String answerTo;

    private Date time;

    @TableField("UserId")
    private String userId;

    @ApiModelProperty(value = "是否为最优答案")
    private Integer accept;

    private Integer praise;



    public String getId() {
        return id;
    }

    public void setId(String Id) {
        this.id = Id;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String Content) {
        this.content = Content;
    }


    public Integer getSupportCnt() {
        return supportCnt;
    }

    public void setSupportCnt(Integer SupportCnt) {
        this.supportCnt = SupportCnt;
    }


    public Integer getOpposeCnt() {
        return opposeCnt;
    }

    public void setOpposeCnt(Integer OpposeCnt) {
        this.opposeCnt = OpposeCnt;
    }


    public String getAnswerTo() {
        return answerTo;
    }

    public void setAnswerTo(String AnswerTo) {
        this.answerTo = AnswerTo;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String UserId) {
        this.userId = UserId;
    }


    public Integer getAccept() {
        return accept;
    }

    public void setAccept(Integer accept) {
        this.accept = accept;
    }


    public Integer getPraise() {
        return praise;
    }

    public void setPraise(Integer praise) {
        this.praise = praise;
    }


}
