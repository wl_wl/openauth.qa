package com.yubao.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpUserReq {

    @ApiModelProperty(value = "用户id")
    private String uid;
    @ApiModelProperty(value = "级别")
    private int rmb;

}
