package com.yubao.test;

import com.alibaba.fastjson.JSON;
import com.yubao.Appllication;
import com.yubao.entity.User;
import com.yubao.request.AddUserReq;
import com.yubao.request.LoginRequest;
import com.yubao.service.CacheService;
import com.yubao.service.UserService;
import com.yubao.util.Const;
import com.yubao.util.NormalException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Appllication.class)
public class TestUser {

    @Autowired
    UserService userInfoService;

    @Autowired
    CacheService cacheService;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setUp(){

        request = new MockHttpServletRequest();
        request.setCharacterEncoding("UTF-8");
        request.addHeader(Const.TOKEN,"");
        response = new MockHttpServletResponse();

    }



    private static final Logger LOGGER = LoggerFactory.getLogger(TestUser.class);
    @Test
    public void add() throws Exception {
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH_mm_ss_SSS");

        AddUserReq req = new AddUserReq();
        req.setAccount("test" + simpleDateFormat1.format(new Date()));
        req.setPwd("01211987");
        req.setName("测试用户");

        userInfoService.regNew(req);

//        AskLog data = service.getById("40536363");
//        LOGGER.info("读取到数据库信息:{}", JSON.toJSONString(data));

    }


    @Test
    public void Login() throws Exception {
        LoginRequest req = new LoginRequest();
        req.setAccount("admin");
        req.setPwd("000000");
        String token = userInfoService.login(req);

        LOGGER.info("登录信息:{}", token);
    }

}
